Project used lombok, therefore for success compilation in IntelliJ Idea you have to turn on annotation processing.

In order to solve the problem set:
    Preferences (Ctrl + Alt + S)
        Build, Execution, Deployment
            Compiler
                Annotation Processors
                    Enable annotation processing

Make sure you have the Lombok plugin for IntelliJ installed!
    Preferences -> Plugins
    Search for "Lombok Plugin"
    Click Browse repositories...
    Choose Lombok Plugin
    Install
    Restart IntelliJ