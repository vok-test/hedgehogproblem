package xyz.ngoar.hedgehog.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.surefire.provider.JUnitPlatformProvider;
import xyz.ngoar.hedgehog.exception.IncorrectInputException;
import xyz.ngoar.hedgehog.utils.ParserUtils;
import xyz.ngoar.hedgehog.data.Size;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserUtilsTest {

    @Test()
    public void parseDataSize_Success() {
        Size size = ParserUtils.parseSize("5 1");
        assertEquals(1, size.getX());
        assertEquals(5, size.getY());
    }

    @Test()
    public void parseDataSize_withExtraWhiteSpaces_Success() {
        Size size = ParserUtils.parseSize(" 13   4");
        assertEquals(4, size.getX());
        assertEquals(13, size.getY());
    }

    @Test()
    public void parseDataSizeTest_IncorrectChar() {
        Assertions.assertThrows(IncorrectInputException.class, () -> {
            Size size = ParserUtils.parseSize("1q 5");
        });
    }

    @Test()
    public void parseDataSizeTest_OneDigit() {
        Assertions.assertThrows(IncorrectInputException.class, () -> {
            Size size = ParserUtils.parseSize(" 1 ");
        });
    }

    @Test()
    public void parseLine_oneValue_Success() {
        int[] row = ParserUtils.parseLine(" 1 ");
        int[] expectedRow = {1};
        assertArrayEquals(row, expectedRow);
    }

    @Test()
    public void parseLine_fewValues_Success() {
        int[] row = ParserUtils.parseLine("2 1 4 6 37 12 999");
        int[] expectedRow = {2, 1, 4, 6, 37, 12, 999};
        assertArrayEquals(row, expectedRow);
    }

    @Test()
    public void parseLine_IncorrectCharacter() {
        Assertions.assertThrows(IncorrectInputException.class, () -> {
            int[] row = ParserUtils.parseLine(" 3 5 7, 1 ");
        });
    }

    @Test()
    public void parseMatrix_Success() throws IOException {
        String matrixDef = "3 2\n" +
                "2 3\n" +
                "3 5\n" +
                "0 1";
        BufferedReader br = new BufferedReader(new StringReader(matrixDef));
        int[][] matrix = ParserUtils.parseMatrix(br);
        validateMatrix(matrix);
    }

    @Test()
    public void parseMatrix_OneByOne_Success() throws IOException {
        String matrixDef =
                "1 1\n" +
                        "2\n";
        BufferedReader br = new BufferedReader(new StringReader(matrixDef));
        int[][] matrix = ParserUtils.parseMatrix(br);
        assertEquals(1, matrix.length);
        assertEquals(1, matrix[0].length);
        assertEquals(2, matrix[0][0]);
    }

    @Test()
    public void parseMatrix_Malformatted_Success() throws IOException {
        String matrixDef = "  3  2\n" +
                "   2 3  \n" +
                " 3   5   \n" +
                "   0   1";
        BufferedReader br = new BufferedReader(new StringReader(matrixDef));
        int[][] matrix = ParserUtils.parseMatrix(br);
        validateMatrix(matrix);
    }

    @Test()
    public void parseMatrix_IncorrectSize_Success() throws IOException {
        Assertions.assertThrows(IncorrectInputException.class, () -> {
            String matrixDef =
                    "3 3" +
                            "2 3\n" +
                            "3 5\n";
            BufferedReader br = new BufferedReader(new StringReader(matrixDef));
            int[][] matrix = ParserUtils.parseMatrix(br);
        });
    }

    @Test()
    public void parseMatrix_IncorrectSize2_Success() throws IOException {
        Assertions.assertThrows(IncorrectInputException.class, () -> {
            String matrixDef =
                    "1 1" +
                            "2 3\n" +
                            "3 5\n";
            BufferedReader br = new BufferedReader(new StringReader(matrixDef));
            int[][] matrix = ParserUtils.parseMatrix(br);
        });
    }

    private void validateMatrix(int[][] matrix) {
        assertArrayEquals(matrix[0], new int[]{2, 3});
        assertArrayEquals(matrix[1], new int[]{3, 5});
        assertArrayEquals(matrix[2], new int[]{0, 1});
    }
}