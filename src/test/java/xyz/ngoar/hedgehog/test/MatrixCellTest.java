package xyz.ngoar.hedgehog.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import xyz.ngoar.hedgehog.data.MatrixCell;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MatrixCellTest {

    @Test
    public void newMatrixCell_withEmptyMatrix(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            MatrixCell cell = new MatrixCell(new int[][] {}, 0,0);
        });
    }

    @Test
    public void getValue(){
        MatrixCell cell = new MatrixCell(new int[][] {{13}}, 0,0);
        assertEquals(13, cell.getValue());
    }

    @Test
    public void getRightCellNull(){
        MatrixCell cell = new MatrixCell(new int[][] {{13}}, 0,0);
        assertNull(cell.getRightCell());
    }

    @Test
    public void getBottomCellNull(){
        MatrixCell cell = new MatrixCell(new int[][] {{13}}, 0,0);
        assertNull(cell.getBottomCell());
    }
}
