package xyz.ngoar.hedgehog.test;

import org.junit.jupiter.api.Test;
import xyz.ngoar.hedgehog.math.HarvestCalculator;
import xyz.ngoar.hedgehog.math.impl.RecursiveHarvestCalculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecursiveHarvestCalculatorTest {

    @Test
    public void calc(){
        HarvestCalculator harvestCalculator = new RecursiveHarvestCalculator();
        int harvest = harvestCalculator.calc(new int[][]{{1,2},{4,6}});
        assertEquals(11, harvest);
    }

    @Test
    public void calc_matrix2by4(){
        HarvestCalculator harvestCalculator = new RecursiveHarvestCalculator();
        int harvest = harvestCalculator.calc(new int[][]{{1,2},{4,6}, {0,0}, {15,1}});
        assertEquals(21, harvest);
    }

    @Test
    public void calc_matrix4by1(){
        HarvestCalculator harvestCalculator = new RecursiveHarvestCalculator();
        int harvest = harvestCalculator.calc(new int[][]{{1, 2, 3, 21}});
        assertEquals(27, harvest);
    }
}