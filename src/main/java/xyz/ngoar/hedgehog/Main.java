package xyz.ngoar.hedgehog;

import lombok.extern.log4j.Log4j2;
import xyz.ngoar.hedgehog.math.HarvestCalculator;
import xyz.ngoar.hedgehog.math.impl.RecursiveHarvestCalculator;
import xyz.ngoar.hedgehog.utils.ParserUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Log4j2
public class Main {

    private static final String INPUT_FILE_NAME = "input.txt";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    public static void main(String[] args) throws IOException {
        HarvestCalculator harvestCalculator = new RecursiveHarvestCalculator();
        log.info("Calculations ....");

        int[][] data = null;
        try (BufferedReader br = Files.newBufferedReader(Paths.get(INPUT_FILE_NAME))) {
            data = ParserUtils.parseMatrix(br);
        }
        int harvest = harvestCalculator.calc(data);
        writeResult(OUTPUT_FILE_NAME, harvest);
        log.info("Completed");
    }

    private static void writeResult(String outputFile, int harvest) throws IOException {
        log.info("Best hedgehog harvest : " + harvest);
        try (BufferedWriter bwr = Files.newBufferedWriter(Paths.get(outputFile))) {
            bwr.write("" + harvest);
        }
    }
}