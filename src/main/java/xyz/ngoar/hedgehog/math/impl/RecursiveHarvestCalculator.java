package xyz.ngoar.hedgehog.math.impl;

import xyz.ngoar.hedgehog.data.MatrixCell;
import xyz.ngoar.hedgehog.math.HarvestCalculator;

/**
 * Thread safe harvest calculator using recursion
 */
public class RecursiveHarvestCalculator implements HarvestCalculator {
    @Override
    public int calc(int[][] matrix) {
        MatrixCell startCell = new MatrixCell(matrix, 0, 0);
        return calc(startCell);
    }

    private int calc(MatrixCell cell) {
        MatrixCell rightCell = cell.getRightCell();
        MatrixCell bottomCell = cell.getBottomCell();

        int rightPath = 0, bottomPath = 0;
        if(rightCell!=null){
            rightPath = calc(rightCell);
        }
        if(bottomCell!=null) {
            bottomPath = calc(bottomCell);
        }

        return cell.getValue() + Math.max(rightPath, bottomPath);
    }
}