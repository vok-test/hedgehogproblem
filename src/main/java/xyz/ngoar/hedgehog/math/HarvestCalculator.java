package xyz.ngoar.hedgehog.math;

public interface HarvestCalculator {
    int calc(int [][] matrix);
}
