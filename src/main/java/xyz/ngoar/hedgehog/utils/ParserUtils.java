package xyz.ngoar.hedgehog.utils;

import org.apache.commons.lang3.StringUtils;
import xyz.ngoar.hedgehog.exception.IncorrectInputException;
import xyz.ngoar.hedgehog.data.Size;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParserUtils {
    public static Size parseSize(String s) {
        List<String> sizeStrList = parseValues(s);

        if (sizeStrList.size() != 2) {
            throw new IncorrectInputException("Incorrect matrix size definition, " + sizeStrList.size() + ", but must be 2");
        }

        try {
            int ySize = Integer.parseInt(sizeStrList.get(0));
            int xSize = Integer.parseInt(sizeStrList.get(1));
            return new Size(xSize, ySize);
        } catch (NumberFormatException e) {
            throw new IncorrectInputException("Incorrect matrix size. " + e.getMessage());
        }
    }

    /**
     * Pases valid values and skips all empty values
     *
     * @param s White space separated string
     * @return list with parsed values
     */
    private static List<String> parseValues(String s) {
        List<String> sizeStrList = new ArrayList<>(Arrays.asList(s.split(" ")));
        sizeStrList.removeIf(String::isEmpty);
        return sizeStrList;
    }

    public static int[] parseLine(String s) {
        List<String> values = parseValues(s);
        int[] row = new int[values.size()];

        int i = 0;
        for (String v : values) {
            try {
                row[i++] = Integer.parseInt(v);
            } catch (NumberFormatException e) {
                throw new IncorrectInputException("Can not parse value in input matrix : " + v);
            }
        }
        return row;
    }

    public static int[][] parseMatrix(BufferedReader br) throws IOException {
        String line = br.readLine();
        //parse matrix size
        if (StringUtils.isEmpty(line)){
            throw new IncorrectInputException("Incorrect matrix size definition");
        }
        Size dataSize = parseSize(line);

        int[][] data = new int[dataSize.getY()][dataSize.getX()];
        int rowCount = 0;
        // read file line by line
        while ((line = br.readLine()) != null) {
            int[] row = parseLine(line);
            if (row.length != dataSize.getX()) {
                throw new IncorrectInputException("Incorrect length of columns in row : " + rowCount);
            }
            data[rowCount++] = row;
        }
        if (data.length != dataSize.getY()) {
            throw new IncorrectInputException("Incorrect length of rows. Required " + dataSize.getY() + ", but is " + data.length);
        }
        return data;
    }
}
