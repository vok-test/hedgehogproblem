package xyz.ngoar.hedgehog.exception;

public class IncorrectInputException extends RuntimeException {
    public IncorrectInputException(String err){
        super(err);
    }
}
