package xyz.ngoar.hedgehog.data;

import lombok.Getter;

/**
 * Pojo describes any 2 dimensional size.
 */
public class Size {
    @Getter
    private int x;
    @Getter
    private int y;

    public Size(int xSize, int ySize) {
        this.x = xSize;
        this.y = ySize;
    }
}