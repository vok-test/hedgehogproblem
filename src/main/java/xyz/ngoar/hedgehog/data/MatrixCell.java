package xyz.ngoar.hedgehog.data;

/**
 * Pojo represents a cell in a matrix.
 * It contains coordinates, value and could return its neighbours: right or bottom.
 */
public class MatrixCell extends Size {
    private Size maxIndexes;
    private int[][] matrix;
    public MatrixCell(int[][] matrix, int xPos, int yPos) {
        super(xPos, yPos);
        if(matrix==null){
            throw new IllegalArgumentException("Matrix is null");
        }

        if(matrix.length==0){
            throw new IllegalArgumentException("Matrix has no no rows");
        }

        if(matrix[0].length==0){
            throw new IllegalArgumentException("Matrix has no no columns");
        }

        this.matrix = matrix;
        this.maxIndexes = new Size(matrix[0].length - 1, matrix.length - 1);
    }

    public MatrixCell getRightCell(){
        if(getX() < maxIndexes.getX()){
            return new MatrixCell(matrix,getX() + 1, getY() );
        }
        return null;
    }

    public MatrixCell getBottomCell(){
        if(getY() < maxIndexes.getY()){
            return new MatrixCell(matrix, getX(), getY() + 1);
        }
        return null;
    }

    public int getValue(){
        return matrix[getY()][getX()];
    }
}